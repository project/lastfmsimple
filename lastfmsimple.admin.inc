<?php

/**
 * @file
 * All functions related to the lastfmsimple adminstration
 */

/**
 * Generates the Last.fm Sipmle Stats admininistration form located at
 * ?q=admin/settings/lastfmsimple.
 */
function lastfmsimple_admin_settings() {
  // Get fields from the profile.module of the type 'textfield'.
  $options = _lastfmsimple_get_textfield_fields();

  // If there aren't any 'textfield' fields, throw an error and return an empty form
  if (empty($options)) {
    drupal_set_message(t('No profile fields of type \'textfield\' were found, please <a href="@profile-settings-page">create one here</a>.', array('@profile-settings-page' => url('admin/user/profile/add/textfield'))), 'error');
    return;
  }

  $options = array('') + $options;
  // Fieldset for general settings
  $form['lastfmsimple_general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['lastfmsimple_general_settings']['lastfmsimple_username_field_id'] = array(
    '#type' => 'select',
    '#title' => t('Profile field'),
    '#default_value' => variable_get('lastfmsimple_username_field_id', NULL),
    '#description' => t("Select which profile field of type 'textfield' you want to use as Last.fm username."),
    '#options' => $options,
    '#required' => TRUE,
  );
  
  // Fieldset for update settings
  $form['lastfmsimple_update_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['lastfmsimple_update_settings']['lastfmsimple_update_method'] = array(
    '#type' => 'radios',
      '#title' => t('Update method'),
      '#description' => t('Whether feeds are updated through Drupal cron or on request. Please note updating on request might cause some slowness.'),
      '#options' => array(
	LASTFMSIMPLE_UPDATE_CRON => t('Drupal cron'),
	LASTFMSIMPLE_UPDATE_REQUEST => t('On request'),
	),
      '#default_value' => variable_get('lastfmsimple_update_method', LASTFMSIMPLE_UPDATE_DEFAULT),
      '#required' => TRUE,
	);
	
  $form['lastfmsimple_update_settings']['lastfmsimple_interval'] = array(
    '#type' => 'select',
      '#title' => t('Update interval'),
      '#description' => t('Select the interval fetched data should be updated. This does not apply when a feed has a usable Expires header.'),
      '#options' => array(
	0 => t('Check always'),
	5 => t('@minutes minutes', array('@minutes' => 5)),
	15 => t('@minutes minutes', array('@minutes' => 15)),
	30 => t('@minutes minutes', array('@minutes' => 30)),
	60 => t('Hourly'),
	120 => t('@hours hours', array('@hours' => 2)),
	180 => t('@hours hours', array('@hours' => 3)),
	360 => t('@hours hours', array('@hours' => 6)),
	720 => t('@hours hours', array('@hours' => 12)),
	1440 => t('Daily'),
	),
      '#default_value' => variable_get('lastfmsimple_interval', 5),
      '#required' => TRUE,
	);

  // Fieldset for the lastfmsimple block.
  $form['lastfmsimple_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Last.fm Simple Stats block settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['lastfmsimple_block']['lastfmsimple_block_stattype'] = array(
    '#type' => 'select',
    '#title' => t('Set Last.fm Simple Stats block statistic type'),
    '#default_value' => variable_get('lastfmsimple_block_stattype', LASTFMSIMPLE_BLOCK_STAT_TYPE_WEEKLYARTISTCHART),
    '#description' => t("Select the statistic type you want to show on the block."),
    '#options' => array(
      LASTFMSIMPLE_BLOCK_STAT_TYPE_WEEKLYARTISTCHART => t('Weekly artist chart'),
      LASTFMSIMPLE_BLOCK_STAT_TYPE_RECENTTRACKS => t('Recent tracks'),
    ),
  );
  
  $form['lastfmsimple_block']['lastfmsimple_statline_display'] = array(
    '#type' => 'radios',
      '#title' => t('Display of stats'),
      '#description' => t('Choose the display type of the lines of statistics.'),
      '#options' => array(
	LASTFMSIMPLE_STATLINE_LINK => t('Link to Last.fm'),
	LASTFMSIMPLE_STATLINE_TEXT => t('Plain text'),
	),
      '#default_value' => variable_get('lastfmsimple_statline_display', LASTFMSIMPLE_STATLINE_DEFAULT),
      '#required' => TRUE,
	);

  return system_settings_form($form);
}
